﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using block.service.Models;
using Newtonsoft.Json;

namespace block.service.Controllers
{
    [Route("api/[controller]")]
    public class BlockChainController : Controller
    {
        private static Blockchain blockChain;
        public static string NodeId { get; private set; }

        static BlockChainController()
        {
            blockChain = new Blockchain();
            NodeId = Guid.NewGuid().ToString();
        }

        public BlockChainController()
        {
        }

        [HttpGet("/chain")]
        public IActionResult GetChain()
        {
            return Ok(blockChain);
        }

        [HttpPost("/mine")]
        public IActionResult Mine()
        {
            Block lastBlock = blockChain.LastBlock;
            string proof = blockChain.ProofOfWork(lastBlock.Proof);

            // Reward minder.
            blockChain.AddTransaction(new Transaction("0", NodeId, 1));

            var block = blockChain.AddBlock(proof);

            return Ok(block);
        }

        [HttpPost("/transactions")]
        public IActionResult PostTransaction([FromBody]Transaction transaction)
        {
            var index = blockChain.AddTransaction(transaction);
            // Add model validation.
            return Ok($"Transaction will be added to Block {index}");
        }


    }
}
