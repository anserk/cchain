using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;

namespace block.service.Models
{
    // public class BlockchainContext : DbContext
    // {
    //     public DbSet<Blockchain> BlockChain { get; set; }

    //     // protected override void OnModelCreating(ModelBuilder modelBuilder)
    //     // {
    //     //     modelBuilder.Entity<Block>()
    //     //         .Property(p =>  [p.Index, p.Proof])
    //     //         .IsRequired;
    //     // }

    // }
    [JsonObject(MemberSerialization.OptIn)]
    public class Blockchain
    {
        [JsonProperty("chain")]
        public List<Block> Blocks { get; set; }
        public List<Transaction> Transactions { get; set; }
        public Blockchain()
        {
            Blocks = new List<Block>();
            Transactions = new List<Transaction>();

            AddBlock("100", "1");
        }

        public Block AddBlock(string proof, string previousHash = "")
        {
            if (String.IsNullOrEmpty(previousHash))
            {
                previousHash = Blockchain.HashBlock(Blocks.Last());
            }

            Block block = new Block(
                Blocks.Count + 1,
                ((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds(),
                proof,
                previousHash,
                Transactions.ToList()
            );
            
            Transactions.Clear();

            Blocks.Add(block);

            return block;
        }

        public int AddTransaction(Transaction transaction)
        {
            Transactions.Add(transaction);

            return LastBlock.Index + 1;
        }

        public static string HashBlock(Block block)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(JsonConvert.SerializeObject(block)));

                foreach (Byte b in result)
                {
                    stringBuilder.Append(b.ToString("x2"));
                }
            }
            return stringBuilder.ToString();
        }

        public static bool IsProofValid(string lastProof, int proof)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (var hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes($"{lastProof}{proof}"));

                foreach (Byte b in result)
                {
                    stringBuilder.Append(b.ToString("x2"));
                }

            }
            string guessHash = stringBuilder.ToString();

            return guessHash.EndsWith("0000");
        }

        public Block LastBlock
        {
            get
            {
                return Blocks.LastOrDefault();
            }
        }

        public string ProofOfWork(string lastProof)
        {
            int proof = 0;
            while (!Blockchain.IsProofValid(lastProof, proof))
            {
                proof += 1;
            }

            return proof.ToString();
        }

        [JsonProperty("length")]
        public int Length
        {
            get
            {
                return Blocks.Count;
            }
        }
    }

    public class Block
    {
        public int Index { get; set; }
        public long UnixTimestamp { get; set; }
        public List<Transaction> Transactions { get; set; }
        public string Proof { get; set; }
        public string PreviousHash { get; set; }

        public Block(
            int index,
            long unixTimestamp,
            string proof,
            string previousHash,
            List<Transaction> transactions
            )
        {
            this.Index = index;
            this.UnixTimestamp = unixTimestamp;
            this.Proof = proof;
            this.PreviousHash = previousHash;

            Transactions = transactions;
        }

    }

    public class Transaction
    {
        // [Required]
        public string Sender { get; set; }
        // [Required]
        public int Amount { get; set; }
        // [Required]
        public string Recipient { get; set; }
        public Transaction(string sender, string recipient, int amount)
        {
            this.Recipient = recipient;
            this.Amount = amount;
            this.Sender = sender;
        }

    }
}